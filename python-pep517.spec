%global _empty_manifest_terminate_build 0
Name:		python-pep517
Version:	0.13.1
Release:	1
Summary:	Wrappers to build Python packages using PEP 517 hooks
License:	MIT
URL:		https://github.com/pypa/pep517
Source0:	https://files.pythonhosted.org/packages/44/d7/8f5d2be1a5fed3b0b5ccd3e800153c0f4dd84c2a688d25bce0bb0cb1f87f/pep517-0.13.1.tar.gz
BuildArch:	noarch

%description
Wrappers to build Python packages using PEP 517 hooks

%package -n python3-pep517
Summary:	Wrappers to build Python packages using PEP 517 hooks
Provides:	python-pep517
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pip
BuildRequires:	python3-wheel
BuildRequires:	python3-pdm-pep517
BuildRequires:	python3-flit-core
Requires:	python3-toml
Requires:	python3-tomli
Requires:	python3-importlib-metadata
Requires:	python3-zipp
%description -n python3-pep517
Wrappers to build Python packages using PEP 517 hooks

%package help
Summary:	Development documents and examples for pep517
Provides:	python3-pep517-doc
%description help
Wrappers to build Python packages using PEP 517 hooks

%prep
%autosetup -n pep517-%{version} -p1

%build
%pyproject_build

%install
%pyproject_install pep517==%{version}
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-pep517 -f filelist.lst
%dir %{python3_sitelib}/*
%{python3_sitelib}/pep517

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Aug 21 2024 xuhe <xuhe@kylinos.cn> - 0.13.1-1
- Update version to 0.13.1
- Try adding python to environments list.
- Remove Python 3.6 from test matrix.
- Don't try to install tox-venv.
- Reword deprecation messages.
- Remove use of pytest-flake8.
- Avoid CI failures by removing pytest-flake8.
- Add warnings on all documentation about pep517 being deprecated.

* Tue Apr 25 2023 caodongxia <caodongxia@h-partners.com> - 0.13.0-3
- Adapting to the pyproject.toml compilation mode

* Fri Mar 24 2023 xu_ping <xuping33@h-partners.com> - 0.13.0-2
- Adaptation to setup.py

* Tue Nov 08 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 0.13.0-1
- Upgrade package to version 0.13.0

* Mon Jul 25 2022 liyanan <liyanan32@h-partners.com> - 0.11.0-3
- Remove python3.10dist(toml) installation dependencies

* Fri Jun 17 2022 liukuo <liukuo@kylinos.cn> - 0.11.0-2
- License compliance rectification

* Thu Jul 22 2021 openstack-sig <openstack@openeuler.org> - 0.11.0-1
- Package Spec generated
